import "./styles.css";
import "label-studio/build/static/css/main.css";
import { useEffect } from "react";
import { initializeLabelStudio } from "./labelStudio.ts";

export default function App() {
  useEffect(() => {
    initializeLabelStudio();
  }, []);

  return (
    <div className="App">
      <h1>Create annotation</h1>
      <div id="label-studio"></div>
    </div>
  );
}
