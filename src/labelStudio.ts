import LabelStudio from "label-studio";

export const initializeLabelStudio = () => {
  const labelStudioConfig = {
    config: `<View>
                <Labels name="label" toName="text">`
                +
                {

                }
                +
                `      <Label value="Person" background="red"/>
                <Label value="City" background="darkorange"/>
                </Labels>
                <Text name="text" value="$text"/>
             </View>`,
    interfaces: [
      "update",
      "submit",
      "controls",
      "side-column",
    ],
    user: {
      pk: 1,
      firstName: "James",
      lastName: "Dean"
    },
    task: {
      annotations: [],
      predictions: [],
      id: 1,
      data: {
        text: "Lalit lives in India"
      }
    },
    onLabelStudioLoad: function (LS) {
      var c = LS.annotationStore.addAnnotation({
        userGenerate: true
      });
      LS.annotationStore.selectAnnotation(c.id);
    }
  };

  const labelStudio = new LabelStudio("label-studio", labelStudioConfig);
};
